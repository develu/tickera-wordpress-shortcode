/**
 * This function outputs a post slider from tc_events
 *
 * @return array
 */
function Get_Tc_events()
{
    //The args you query to wordpress db.
    $args = array(
        'post_type' => 'tc_events',//Your custom post type name
        'post_status' => 'publish',//Check if published
        'tax_query' => array(
            array(
                'taxonomy' => 'event_category',
                'field' => 'slug',
                'terms' => array('Artimiausi'),//Subcategory name
            ),
        ),
    );
    $tcCustomEventsObj = new WP_Query($args);//Where the query magic happens

    $tcObjContainer = $tcCustomEventsObj->posts;//Take only posts from the object

    echo '<div class="container-slider rkc-slider-container">';//Any custom html for styling.
    echo '<div class="flexslider-2 carousel">';
    echo '<ul class="slides">';

    foreach ($tcObjContainer as $post) {//Loop through the array of posts

        $title = $post->post_title;//Take post title.
        $link = $post->guid;//Take post URL
        $post_ID = $post->ID;//Take post id
        $date = do_shortcode('[tc_event_date event_id="' . $post_ID . '"]');//The plugin has unique way of working with dates, the workaround is to work with their shortcode, core editing would be messed up with update.
        //Date manipulation code is used to print date in a client specified way.
        $date = explode(" - ", $date);//Split YYYY-MM-DD - YYYY-MM-DD to two parts
        $first_date = date_parse_from_format("Y-m-d G:i", $date[0]);//First date part
        $second_date = date_parse_from_format("Y-m-d G:i", $date[1]);//Second date part
        $formatedFirstDate = date("Y-m-d", strtotime($first_date['year'] . '-' . $first_date['month'] . '-' . $first_date['day']));//Change date format
        $formatedSecondDate = date("Y-m-d", strtotime($second_date['year'] . '-' . $second_date['month'] . '-' . $second_date['day']));//Change date format
        $months = array("Sausis", "Vasaris", "Kovas", "Balandis", "Gegužė", "Birželis", "Liepa", "Rugpjūtis", "Rugsėjis", "Spalis", "Lapkritis", "Gruodis");//This array used to sort out wanted categories from wordpress

        echo '<li>';//Formating custom html to frontend
        echo '<a href="' . $link . '">';
        echo '<h3>' . $title . '</h3>';
        echo '<p class="rkc-line-date">';
        if ($first_date == $second_date || $second_date['year'] == false) { //If second date is equal to first date then the event is going to take only one day so we only print one date.
            echo $date[0];
        } else if ($first_date['year'] >= $second_date['year'] && $first_date['month'] >= $second_date['month'] && $second_date['day'] > $first_date['day']) {//If event is longer the one day we print the range.
            echo $formatedFirstDate . ' - ' . $formatedSecondDate;
        }
        echo '</p>';
        echo '</a>';
        echo '</li>';
    }

    echo '</ul>';
    echo '</div>';
    echo '<div class="rkc-month-line">';

    foreach ($months as $month) {//Loop through the months array
        $event_category_term = get_term_by('name', $month, 'event_category');//Find the custom subcategories in wordpress custom post type Object
        echo '<a class="month-button" href="' . esc_url(get_category_link($event_category_term->term_id)) . '">' . $month . '</a>';
    }

    echo '</div>';
    echo "</div>";
}
add_shortcode('tc_events_shortcode', 'get_tc_events');// This allows us to call the code in a place where we need it.
