# Tickera Wordpress Shortcode

Output custom Tickera posts or any custom post type to a shortcode.

The code is used inside Wordpress child or main theme function.php file.

The code is tailored to be used with Ticker plugin.

The css is not included.

Tested with 4.9 to 5.2 versions of wordpress and according version of Ticker plugin.

Can be easily used with custom post type or even Wordpress posts, the second case doesn't make sense since wordpress has alot of dedicated tools for that.